// a library to wrap and simplify api calls
import apisauce from 'apisauce';
import { GlobalConst } from '../Themes';

// our "constructor"
const create = (baseURL = GlobalConst.url) => {
  // ------
  // STEP 1
  // ------
  //
  // Create and configure an apisauce-based api object.
  //
  const api = apisauce.create({
    // base URL is read from the "constructor"
    baseURL,
    // here are some default headers
    headers: {
      'Cache-Control': 'no-cache',
      'Content-Type': 'application/json'
    },
    // 10 second timeout...
    timeout: 600000
  })

  // ------
  // STEP 2
  // ------
  //
  // Define some functions that call the api.  The goal is to provide
  // a thin wrapper of the api layer providing nicer feeling functions
  // rather than "get", "post" and friends.
  //
  // I generally don't like wrapping the output at this level because
  // sometimes specific actions need to be take on `403` or `401`, etc.
  //
  // Since we can't hide from that, we embrace it by getting out of the
  // way at this level.
  //
  // const getRoot = () => api.get('')
  // const getRate = () => api.get('rate_limit')
  // const getUser = (username) => api.get('search/users', {q: username})
  // const register = (data) => api.get('api_users/profile/format/json', data );

  const register = (data) => api.post('api_users/register/format/json', data, {headers: {'Content-Type': 'application/json', 'Accept': 'application/json'}})
  const login = (data) => api.post('api_users/login/format/json', data, {headers: {'Content-Type': 'application/json', 'Accept': 'application/json'}})
  const getuserProfile = (data) => api.post('api_users/edit_profile_get/format/json', data, {headers: {'Content-Type': 'application/json', 'Accept': 'application/json'}})
  const updateUserProfile = (data) => api.post('api_users/edit_profile/format/json', data, {headers: {'Content-Type': 'application/json', 'Accept': 'application/json'}})
  const getteam = (data) => api.post('api_users/team/format/json', data, {headers: {'Content-Type': 'application/json', 'Accept': 'application/json'}})
  const updateUserStatus = (data) => api.post('api_users/status/format/json', data, {headers: {'Content-Type': 'application/json', 'Accept': 'application/json'}})

  // ------
  // STEP 3
  // ------
  //
  // Return back a collection of functions that we would consider our
  // interface.  Most of the time it'll be just the list of all the
  // methods in step 2.
  //
  // Notice we're not returning back the `api` created in step 1?  That's
  // because it is scoped privately.  This is one way to create truly
  // private scoped goodies in JavaScript.
  //
  return {
    // a list of the API functions from step 2
    // getRoot,
    // getRate,
    // getUser,
    register,
    login,
    getuserProfile,
    updateUserProfile,
    updateUserStatus,
    getteam
  }
}

// let's return back our create method as the default.
export default {
  create
}
