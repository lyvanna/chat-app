import React, { Component } from 'react';
import { Spinner } from 'native-base';
import { Colors } from '../Themes'
export default class LoadingSpinner extends Component {
  constructor(props){
    super(props);
    this.state={
    }
}

  render() {
    return (
        <Spinner color={Colors.main_color} />
    );
  }
}