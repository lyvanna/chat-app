import React,{Component} from 'react';
import {AsyncStorage} from 'react-native';
import { Router, Scene } from 'react-native-router-flux';
import { connect } from 'react-redux'
import setFooterActions from '../Redux/SetFooterRedux';
import LaunchScreen from "../Containers/LaunchScreen";
import LoginAndRegisterScreen from "../Containers/LoginAndRegisterScreen";
import HomeScreen from "../Containers/HomeScreen";

class NavigationRouter extends Component{
    constructor(props){
        super(props);
        this.state = {
            group_id: 2
        }
    }
    
    componentDidMount() {
        AsyncStorage.multiGet(['group_id']).then(stores => {
            this.setState({
                group_id: stores[0][1]
            });
        });
    }

    onEnter=(withFooter)=>{
        this.props.setFooter(withFooter)
    }
    
    render(){
        return(
            <Router>
                <Scene key = "root">
                    <Scene type="reset" initial={true} onEnter={()=>this.onEnter(true)} key = "HomeScreen" component = {HomeScreen} hideNavBar={true} />
                </Scene>
            </Router>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
      setFooter: (data) => dispatch(setFooterActions.setFooterRequest(data)),
    }
}
  
export default connect(null, mapDispatchToProps)(NavigationRouter)