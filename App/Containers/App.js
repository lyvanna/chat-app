import '../Config';
import React, { Component } from 'react';
import { Provider } from 'react-redux';
import createStore from '../Redux';
import { GlobalConst } from '../Themes';
import LaunchScreen from './LaunchScreen';
import { Actions } from 'react-native-router-flux';
import OneSignal from 'react-native-onesignal';
import codePush from "react-native-code-push";
let codePushOptions = { checkFrequency: codePush.CheckFrequency.ON_APP_RESUME };

// create our store
const store = createStore();
class App extends Component {
  constructor(props) {
    super(props);
    OneSignal.init(GlobalConst.onesignal_key);
    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
    OneSignal.setLocationShared(false);
    OneSignal.inFocusDisplaying(2);
  }

  componentDidMount() {
  }

  componentWillUnmount() {
    // OneSignal.removeEventListener('received', this.onReceived);
    // OneSignal.removeEventListener('opened', this.onOpened);
  }

  onReceived(notification) {
    Actions.HomeScreen({ type: 'reset' });
  }

  onOpened(openResult) {
    Actions.HomeScreen({ type: 'reset' });
  }
  
  render () {
    return (
      <Provider store={store}>
        <LaunchScreen/>
      </Provider>
    )
  }
}

export default codePush(codePushOptions)(App)