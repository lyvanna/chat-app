import React,{Component} from 'react';
import {View,StyleSheet, AsyncStorage, Image, TouchableOpacity, Modal, ScrollView} from 'react-native';
import { Icon, Content, Container, List, ListItem, Thumbnail, Text, Left, Body, Right, Separator, Button, Input, Textarea } from 'native-base';
import UserProfileActions from '../Redux/UserProfileRedux';
import { connect } from 'react-redux';
import { GlobalConst, Colors, GlobalStyles } from '../Themes';
import LoadingSpinner from '../Components/LoadingSpinner';
import ImagePicker from 'react-native-image-crop-picker';
import PinView from 'react-native-pin-view';
import { Actions } from 'react-native-router-flux';
import Geolocation from 'react-native-geolocation-service';

isUpdate = true;
class SettingScreen extends Component{
    constructor(props){
        super(props);
        this.state={
            userData: [],
            name: '',
            phone: '',
            password: '',
            user_id: '',
            group_id: 2,
            LoadingStatus: true,
            image1: '',
            image2: '',
            image3: '',
            image4: '',
            errorMessage: null,
            loading: false,
            phone2: '',
            actionPopup: false,
            action_id: 0,
            isUserUpdateProfileRequest: false,
            old_pin: 0,
            PINPopup: false,
            pin: 0,
            pin_step: 0,
            isProfile: true,
            latitude: '',
            longitude: ''
        }
    }

    componentDidMount = () =>{
        isUpdate = true;
        AsyncStorage.multiGet(['user_id', 'group_id']).then(stores => {
            user_id = stores[0][1];
            group_id = stores[1][1];
            this.setState({
                user_id: user_id,
                group_id: group_id
            });
            this.props.requestUserProfile({key: GlobalConst.key, user_id: user_id});
        });

        Geolocation.getCurrentPosition(
            (position) => { 
                this.setState({
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude
                });
            },
            (error) => { },
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000, distanceFilter: 10 }
        );
    }
    
    componentWillReceiveProps = (newProps) =>{
        if (isUpdate && newProps.getUserProfile && newProps.getUserProfile.fetching == false ) {
            if (newProps.getUserProfile.payload && newProps.getUserProfile.payload.data) {
                userData = newProps.getUserProfile.payload.data;
                this.setState({
                    isProfile: false,
                    LoadingStatus: false,
                    userData: userData,
                    name: userData.name,
                    phone: userData.phone,
                    phone2: userData.phone2,
                    photo1: userData.photo1,
                    photo2: userData.photo2,
                    image1: userData.image1,
                    image2: userData.image2,
                    image3: userData.image3,
                    image4: userData.image4,
                    address: userData.address
                });
            }
        }

        if (this.state.isUserUpdateProfileRequest && newProps.userUpdateProfileRequest && newProps.userUpdateProfileRequest.fetching == false ) {
            if (isUpdate==true && newProps.userUpdateProfileRequest.payload && newProps.userUpdateProfileRequest.payload.data) {
                getValueRegister = newProps.userUpdateProfileRequest.payload.data;
                this.setState({
                    LoadingStatus: false,
                    isUserUpdateProfileRequest: false
                });

                if(getValueRegister.msgError){
                    this.setState({
                        msgError: getValueRegister.msgError
                    });
                }else{
                    this.setState({actionPopup: false});
                    this.props.requestUserProfile({key: GlobalConst.key, user_id: this.state.user_id});
                }
            }
        }
    }

    handleUpdateProfile = () =>
    {
        isUpdate = true;
        this.setState({
            LoadingStatus: true,
            isUserUpdateProfileRequest: true,
        });

        this.props.UpdateUserProfile({
            key: GlobalConst.key, 
            user_id: this.state.user_id,
            ref_id: this.state.ref_id, 
            phone: this.state.phone, 
            name: this.state.name,  
            password: this.state.password,
            phone2: this.state.phone2,
            address: this.state.address,
            lat: this.state.latitude,
            lon: this.state.longitude
        });
    }

    handleEditPhoto = (belongPhotoName) =>
    {
        isUpdate = false;
        ImagePicker.openPicker({
            cropping: true,
            includeBase64: true,
            compressImageMaxWidth: 500
        }).then(response => {
            var sourceImage = "data:"+response.mime+";base64," + response.data;
            sourceImage = sourceImage.trim();

            if(belongPhotoName == "cover"){
                this.setState({photo2: sourceImage});
                this.props.UpdateUserProfile({
                    key: GlobalConst.key, 
                    user_id: this.state.user_id, 
                    photo2:sourceImage
                });
            }else{
                this.setState({photo1: sourceImage});
                this.props.UpdateUserProfile({
                    key: GlobalConst.key, 
                    user_id: this.state.user_id, 
                    photo1:sourceImage
                });
            }
        });
    }

    chooseDataImage = (belongDataImage) =>
    {
        isUpdate = false;
        ImagePicker.openPicker({
            cropping: true,
            includeBase64: true,
            compressImageMaxWidth: 500
        }).then(response => {
            var sourceImage = "data:"+response.mime+";base64," + response.data;
            sourceImage = sourceImage.trim();

            if(belongDataImage == "image1"){
                this.setState({image1: sourceImage});
                this.props.UpdateUserProfile({key: GlobalConst.key, user_id: this.state.user_id, image1:sourceImage});
            } else if(belongDataImage == "image2"){
                this.setState({image2: sourceImage});
                this.props.UpdateUserProfile({key: GlobalConst.key, user_id: this.state.user_id, image2:sourceImage});
            } else if(belongDataImage == "image3"){
                this.setState({image3: sourceImage});
                this.props.UpdateUserProfile({key: GlobalConst.key, user_id: this.state.user_id, image3:sourceImage});
            }else if(belongDataImage == "image4"){
                this.setState({image4: sourceImage});
                this.props.UpdateUserProfile({key: GlobalConst.key, user_id: this.state.user_id, image4:sourceImage});
            }
        });
    }

    handlePhone = (value) => {
        if (/^\d+$/.test(value)) {
          this.setState({
            phone: value
          });
        }
        if(value=='')
        {
            this.setState({
                phone: value
              });
        }
    }

    handleUserName = (value) => {
        this.setState({
            name: value
        });
    }

    handleAddress = (value) => {
        this.setState({
            address: value
        });
    }

    handlePhone2 = (value) => {
        if (/^\d+$/.test(value)) {
            this.setState({
                phone2: value
            });
        }
        if(value=='')
        {
            this.setState({
                phone2: value
            });
        }
    }

    handleActionPopup = (action_id)=>{
        if(this.state.userData.verify==1 && action_id=='name') return false;
        
        this.setState({
            action_id: action_id,
            actionPopup: true
        });
    }
    handleCloseActionPopup = ()=>{ //BACK <-
        this.setState({actionPopup: false});
        Actions.SettingScreen();
    }

    handlePin = ()=>{
        pin_step = 2;
        if(this.state.userData.pin) pin_step = 1;

        this.setState({
            PINPopup: true,
            pin_step: pin_step
        });
    }

    onComplete = (val, clear)=> {
        if(this.state.pin_step==1){
            if(val == this.state.userData.pin){
                this.setState({
                    pin_step: 2,
                    old_pin: val
                });
            }
        }else if(this.state.pin_step==2){
            this.setState({
                pin_step: 3,
                pin: val
            });
        }else if(this.state.pin_step==3){
            if(this.state.pin==val){
                isUpdate = true;
                this.setState({
                    LoadingStatus: true,
                    isUserUpdateProfileRequest: true,
                    PINPopup: false
                });

                this.props.UpdateUserProfile({
                    key: GlobalConst.key, 
                    user_id: this.state.user_id,
                    old_pin: this.state.old_pin,
                    pin: val
                });
            }
        }
        clear();
    }

    handleClosePINPopup = ()=>{
        this.setState({PINPopup: false});
    }

    render(){
        return(
            this.state.LoadingStatus==true?
                <LoadingSpinner/>
            :
            <Container>
                <Content>
                    <View style={styles.HeaderCover}>
                            <TouchableOpacity onPress={() => this.handleEditPhoto("cover")} style={{width: '100%'}}>
                                <Image source={{uri: this.state.photo2}} style={{ width: '100%', height: 200}}/>
                            </TouchableOpacity>
                    </View>
                
                    <View style={styles.HeaderProfile}>
                        <View style={styles.HeaderProfileImage}>
                            <TouchableOpacity onPress={() => this.handleEditPhoto("profile")} style={{width:'100%'}}>
                                <Thumbnail circle source={{uri: this.state.photo1}} style={{ width: '100%', height: 150}}/>                 
                            </TouchableOpacity>
                        </View>
                    </View>

                    <List>
                        <ListItem icon>
                            <Left><Icon name="card" active /></Left>
                            <Body><Text>ID: {this.state.userData.user_ids}</Text></Body>
                            {this.state.userData.verify==1?<Right><Icon name="checkmark-circle" style={{color: Colors.main_color}} active /></Right>:null}
                        </ListItem>

                        <ListItem icon noBorder onPress={() => this.handleActionPopup('name')}>
                            <Left><Icon name="person" active /></Left>
                            <Body><Text>{this.state.name?this.state.name:'Full name'}</Text></Body>
                            {this.state.userData.verify!=1?<Right><Icon name="arrow-forward" active/></Right>:null}
                        </ListItem>

                        <Separator bordered><Text>Security</Text></Separator>
                        <ListItem icon onPress={() => this.handleActionPopup('password')}>
                            <Left><Icon name="key" active /></Left>
                            <Body><Text>Change Password</Text></Body>
                            <Right><Icon name="arrow-forward" active/></Right>
                        </ListItem>

                        <ListItem icon noBorder onPress={() => this.handlePin()}>
                            <Left><Icon name="finger-print" active style={{color: 'red'}} /></Left>
                            <Body><Text>Change PIN</Text></Body>
                            <Right><Icon name="arrow-forward" active/></Right>
                        </ListItem>

                        <Separator bordered><Text>Contact</Text></Separator>
                        <ListItem icon>
                            <Left><Icon name="call" active /></Left>
                            <Body><Text>{this.state.phone}</Text></Body>
                        </ListItem>

                        <ListItem icon onPress={() => this.handleActionPopup('phone2')}>
                            <Left><Icon name="call" active /></Left>
                            <Body><Text>{this.state.phone2}</Text></Body>
                            <Right><Icon name="arrow-forward" active/></Right>
                        </ListItem>

                        <ListItem icon noBorder onPress={() => this.handleActionPopup('address')}>
                            <Left><Icon name="pin" active /></Left>
                            <Body><Text>{this.state.address}</Text></Body>
                            <Right><Icon name="arrow-forward" active/></Right>
                        </ListItem>

                        <Separator bordered><Text>Document</Text></Separator>
                        <View style={{flexDirection: 'row', alignItems:'center',justifyContent:'space-between', paddingTop: 20, paddingBottom:20}}>
                            <TouchableOpacity style={[styles.boxImage,{marginRight: 5}]} onPress={() => this.chooseDataImage("image1")}>
                                <Image style={styles.imagePro} source={{uri: this.state.image1}} />
                            </TouchableOpacity>

                            <TouchableOpacity style={[styles.boxImage,{marginLeft:5}]} onPress={() => this.chooseDataImage("image2")}>
                                <Image style={styles.imagePro} source={{uri: this.state.image2}} />
                            </TouchableOpacity>
                        </View>

                        <View style={{flexDirection: 'row', alignItems:'center',justifyContent:'space-between', paddingBottom:20}}>
                            <TouchableOpacity style={[styles.boxImage,{marginRight: 5}]} onPress={() => this.chooseDataImage("image3")}>
                                <Image style={styles.imagePro} source={{uri: this.state.image3}} />
                            </TouchableOpacity>

                            <TouchableOpacity style={[styles.boxImage,{marginLeft:5}]} onPress={() => this.chooseDataImage("image4")}>
                                <Image style={styles.imagePro} source={{uri: this.state.image4}} />
                            </TouchableOpacity>
                        </View>
                    </List>

                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={this.state.actionPopup}
                        onRequestClose={()=>this.handleCloseActionPopup}>
                        <View style={styles.popupView}>
                            <ScrollView>
                                <View>
                                    <TouchableOpacity onPress={() => this.handleCloseActionPopup()}>
                                        <Icon active name="arrow-round-back" style={{fontSize:32, color: Colors.main_color}}/>
                                    </TouchableOpacity>
                                </View>

                                {this.state.action_id=='name'?
                                    <View style={GlobalStyles.frmDiv}>
                                        <Input style={GlobalStyles.inputBox}
                                            value={this.state.name}
                                            placeholder="Name"
                                            onChangeText={this.handleUserName}
                                        />
                                    </View>
                                :null}

                                {this.state.action_id=='password'?
                                    <View style={GlobalStyles.frmDiv}>
                                        <Input style={GlobalStyles.inputBox}
                                            value={this.state.password}
                                            placeholder="******"
                                            secureTextEntry={true}
                                            onChangeText={(text) => this.setState({password: text})}
                                        />
                                    </View>
                                :null}

                                {this.state.action_id=='pin'?
                                    <View>
                                        <View style={GlobalStyles.frmDiv}>
                                            <Input style={GlobalStyles.inputBox}
                                                value={this.state.old_pin}
                                                placeholder="Old PIN"
                                                secureTextEntry={true}
                                                keyboardType = 'numeric'
                                                maxLength={4}
                                                onChangeText={(text) => this.setState({old_pin: text})}
                                            />
                                        </View>

                                        <View style={GlobalStyles.frmDiv}>
                                            <Input style={GlobalStyles.inputBox}
                                                value={this.state.pin}
                                                placeholder="New PIN"
                                                secureTextEntry={true}
                                                keyboardType = 'numeric'
                                                maxLength={4}
                                                onChangeText={(text) => this.setState({pin: text})}
                                            />
                                        </View>
                                    </View>
                                :null}

                                {this.state.action_id=='phone2' || this.state.action_id=='address'?
                                    <View>
                                        <View style={GlobalStyles.frmDiv}>
                                            <Input style={GlobalStyles.inputBox}
                                                value={this.state.phone2}
                                                onChangeText={this.handlePhone2}
                                                keyboardType = 'numeric'
                                                placeholder= 'Phone #1'
                                            />
                                        </View>
                                        <View style={GlobalStyles.frmDiv}>
                                            <Textarea 
                                                rowSpan={3} 
                                                bordered 
                                                placeholder="Address" 
                                                onChangeText={this.handleAddress}
                                                value = {this.state.address}
                                            />
                                        </View>
                                    </View>
                                :null}
                            </ScrollView>

                            <View style={{justifyContent:'center', marginTop:25, flexDirection:'row', flex:1}}>
                                <Button danger rounded onPress={this.handleUpdateProfile}>
                                    <Text>SAVE</Text>
                                </Button>
                            </View>

                            <View style={GlobalStyles.frmHolder}>
                                {this.state.msgError?
                                    <View style={{alignItems:'center'}}>
                                        <Text style={{fontSize:15,color:'red'}}>{this.state.msgError}</Text>
                                    </View>
                                :null}
                            </View>
                        </View>
                    </Modal>

                    <Modal
                        transparent={true}
                        visible={this.state.PINPopup}
                        onRequestClose={()=>this.handleClosePINPopup}>
                        <View style={styles.popupView}>
                            <View style={{height: 40,flexDirection: 'row', alignItems:'flex-end',justifyContent:'flex-end'}}>
                                <TouchableOpacity onPress={this.handleClosePINPopup} style={{width: 40, height: 40, borderRadius: 100, backgroundColor: "red", justifyContent: 'center', alignItems: 'center'}}>
                                    <Icon name="close" size={15} style={{color: '#fff'}} />
                                </TouchableOpacity>
                            </View>
                            <View style={{alignItems: 'center', padding: 25}}>
                                {this.state.pin_step==1?
                                    <Text style={{fontSize: 28, fontWeight:'bold'}}>Old PIN</Text>
                                :null}
                                {this.state.pin_step==2?
                                    <Text style={{fontSize: 28, fontWeight:'bold'}}>New PIN</Text>
                                :null}
                                {this.state.pin_step==3?
                                    <Text style={{fontSize: 28, fontWeight:'bold'}}>Confirm PIN</Text>
                                :null}
                            </View>
                            <PinView
                                onComplete={this.onComplete}
                                pinLength={4} // You can also use like that.
                            />
                        </View>
                    </Modal>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    holderFm:{
        marginTop: 10
    },
    HeaderCover:
    {
        width: "100%", 
        height: 200, 
        alignItems:'center',
        justifyContent:'center',
        backgroundColor: Colors.main_color
    },
    HeaderProfile:
    {
        alignItems:'center',
        justifyContent:'center',
        backgroundColor: '#efefef'
    },
    HeaderProfileImage:
    {
        marginTop: -75,
        width: 150, 
        height: 150, 
        alignItems:'center',
        justifyContent:'center',
        backgroundColor: '#efefef'
    },
    HeaderId:
    {
        flexDirection: 'row', 
        alignItems:'center',
        justifyContent:'center'
    },
    IdTxt:{
        color: Colors.main_color,
        fontWeight: 'bold'
    },
    imagePro: {
        width: "100%",
        height: "100%"
    },
    boxImage: {
        width: "50%",
        height: 150, 
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:'lightgray'
    },
    popupView: {
        width: "100%", 
        height: '100%',
        padding: 25, 
        backgroundColor:'#fff'
    }
});

const mapStateToProps = (state) => {
    return {
      getUserProfile: state.userprofile,
      userUpdateProfileRequest: state.updateprofile
    }
}
  
const mapDispatchToProps = (dispatch) => {
    return {
        requestUserProfile: (data) => dispatch(UserProfileActions.userProfileRequest(data)),
        UpdateUserProfile: (data) => dispatch(UserProfileActions.userUpdateProfileRequest(data))
    }
}
  
export default connect(mapStateToProps, mapDispatchToProps)(SettingScreen)