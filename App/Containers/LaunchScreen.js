import '../Config';
import React, { Component } from 'react';
import { StyleSheet, View, StatusBar, AsyncStorage, Image, YellowBox } from 'react-native';
import { connect } from 'react-redux';
import FooterScreen from './Layouts/FooterScreen';
import RootContainer from './RootContainer';
import { Container } from 'native-base';
import LoginAndRegisterScreen from './LoginAndRegisterScreen';
import setFooterActions from '../Redux/SetFooterRedux';
import { Images, Colors } from '../Themes';
// create our store

class LaunchScreen extends Component {
  constructor(props){
    super(props);
    this.state = {
      WelcomeScreen: true,
      LoginStatus: null
    };
  }
  componentDidMount = () => {
    setTimeout(function () {
      this.setState({ WelcomeScreen: false });
      StatusBar.setBackgroundColor(Colors.main_color, true)
    }.bind(this), 1000);

    AsyncStorage.getItem('LoginStatus').then((value)=>{
      this.setState({ LoginStatus: value });
    });
  };
    
  componentWillReceiveProps = (newProps) =>{
    if(newProps.getcookielogin)
    {
      this.setState({ LoginStatus: 'true' });
    }
  }
  render () {
    YellowBox.ignoreWarnings(['Warning:']);
    const StatusBarAPP = (
      <StatusBar backgroundColor={Colors.main_color} barStyle="light-content"/>
    );

    return (
      <View style={styles.container}>
        {StatusBarAPP}
        <View style={styles.content}>
          {this.state.WelcomeScreen?
            <View style={{ padding: 20, position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, justifyContent: 'center', alignItems: 'center'}}>
                <Image resizeMode="contain" source={Images.logo} style={{ width: '100%', height: 300, justifyContent: 'center'}}/>
            </View>
          :
          this.state.LoginStatus==null ?
            <LoginAndRegisterScreen/>
          :
            <Container>
              <RootContainer />
              <FooterScreen />
            </Container>
          }
        </View>
      </View>
    )
  }
}
const STATUSBAR_HEIGHT = StatusBar.currentHeight;
const APPBAR_HEIGHT = 56;
const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  statusBar: {
    height: STATUSBAR_HEIGHT
  },
  appBar: {
    backgroundColor: Colors.main_color,
    height: APPBAR_HEIGHT,
    justifyContent:'center'
  },
  content: {
    flex: 1,
    backgroundColor: Colors.main_color
  }
});

const mapStateToProps = (state) => {
  return {
    getcookielogin: state.cookielogin.payload
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    setFooter: (data) => dispatch(setFooterActions.setFooterRequest(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LaunchScreen)