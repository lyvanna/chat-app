import React,{Component} from 'react';
import { View, Text, Icon } from 'native-base';
import {Colors} from '../Themes';

export default class SuccessScreen extends Component{
    constructor(props){
        super(props);
    }

    render(){
        return(
            <View style={{padding:15}}>
                <Text style={{fontSize: 28}}>The action is completed.</Text>
                <View style={{alignItems:'center', padding: 15}}>
                    <Icon name="checkmark-circle" style={{color: Colors.main_color, fontSize:42}} active/>   
                </View>
            </View>
        );
    }
}