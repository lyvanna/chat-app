import React,{Component} from 'react';
import {View,StyleSheet, AsyncStorage, TouchableOpacity, Switch, Platform} from 'react-native';
import {Icon, Text, Content, Container} from 'native-base';
import { Colors, GlobalConst } from '../Themes';
import { connect } from 'react-redux';
import LogoutActions from '../Redux/LogoutRedux';
import userStatusActions from '../Redux/UserStatusRedux';
import UserProfileActions from '../Redux/UserProfileRedux';
import LoadingSpinner from '../Components/LoadingSpinner';
import ActionExitApp from 'react-native-exit-app';
import { Actions } from 'react-native-router-flux';
import RequestingTypesActions from '../Redux/RequestingRedux';

class MoreOptionsScreen extends Component{
    constructor(props){
        super(props);
        this.state={
            user_id: '',
            status: 1,
            stype: 1,
            group_id: 2,
            loading: true,
            statusLoading: true
        }
    }

    componentDidMount(){
        AsyncStorage.multiGet(['user_id', 'group_id', 'stype']).then(stores => {
            user_id = stores[0][1];
            group_id = stores[1][1];
            stype = stores[2][1];
            this.setState({
                user_id: user_id,
                group_id: group_id,
                stype: stype
            });
            this.props.requestUserProfile({key: GlobalConst.key, user_id: user_id});
        });
    }

    componentWillReceiveProps = (newProps) =>{
        if (this.state.statusLoading && newProps.getUserProfile && newProps.getUserProfile.fetching==false){
            if (newProps.getUserProfile.payload && newProps.getUserProfile.payload.data) {
                userData = newProps.getUserProfile.payload.data;
                this.setState({
                    userData: userData,
                    status: userData.status,
                    stype: userData.stype,
                    loading: false,
                    statusLoading: false
                });
            }
        }

        if (newProps.getlogout && newProps.getlogout.fetching==false){
            if (newProps.getlogout.payload && newProps.getlogout.payload.data) {
                logout_data = newProps.getlogout.payload.data;
                if(logout_data.stype){
                    AsyncStorage.setItem('stype', logout_data.stype.toString());
                }else{
                    AsyncStorage.multiRemove(['LoginStatus', 'user_id', 'group_id', 'stype', 'player_id']);
                }

                alert("Exit App");
                setTimeout(function () {
                    ActionExitApp.exitApp();
                }.bind(this), 1000);
            }
        }

        if (this.state.is_oo && newProps.userstatus && newProps.userstatus.fetching==false){
            if (newProps.userstatus.payload && newProps.userstatus.payload.data) {
                this.props.requestUserProfile({key: GlobalConst.key, user_id: user_id});
                this.setState({is_oo: false});
            }
        }
    }

    handleAgentScreen (){
        Actions.AgentScreen();
    }

    handlePaymentScreen (){
        Actions.FeeScreen();
    }

    handleTransaction = ()=>{
        Actions.TransactionsScreen();
    }

    handleContactScreen (){
        Actions.ContactScreen();
    }

    handleTosScreen (){
        Actions.TosScreen();
    }

    handleSettingScreen (){
        Actions.SettingScreen();
    }

    handleLogout = () =>
    {
        this.setState({loading: true});
        this.props.logout({key: GlobalConst.key, user_id: this.state.user_id});
    }

    SwitchUserStatus = (value)=>{
        if(value==true){
            this.setState({status: 2});
        }else{
            this.setState({status: 1});
        }
        this.props.updateUserStatus({key: GlobalConst.key, user_id: this.state.user_id, status: this.state.status==2?1:2});
        this.setState({is_oo: true});
    }

    render(){
        return(
            <Container>
                <Content>
                    <View style={{flex:1, padding: 15}}>
                        <View style={{justifyContent:'center', flexDirection: 'row', paddingBottom:15 }}>
                            {this.state.loading==true?
                                <LoadingSpinner/>
                            :
                                <View>
                                    <Switch
                                    style={{...Platform.select({ ios: { transform: [{ scaleX: 1.2 }, { scaleY: 1.2 }] }, android: {transform: [{ scaleX: 1.5 }, { scaleY: 1.5 }]} })}}
                                    onValueChange={(value)=>this.SwitchUserStatus(value)}
                                    thumbColor={this.state.status!=2?"grey":Colors.main_color}
                                    trackColor={Platform.select({ ios: "grey", android: ""})}
                                    value={this.state.status!=2?false:true}
                                    />
                                </View>
                            }
                        </View>

                        <TouchableOpacity onPress={this.handlePaymentScreen} style={styles.TouchableOpacityStyle}>
                            <Icon type="Ionicons" name="pricetag" style={[styles.IconStyle,{paddingLeft: 5}]} active/>
                            <Text style={styles.TextStyle}>Service fee / តំលៃសេវាកម្ម</Text>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={this.handleTransaction} style={styles.TouchableOpacityStyle}>
                            <Icon type="Ionicons" name="md-notifications" style={[styles.IconStyle,{paddingLeft: 5}]} active/>
                            <Text style={styles.TextStyle}>Transaction / ប្រតិបត្តការ</Text>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={this.handleSettingScreen} style={styles.TouchableOpacityStyle}>
                            <Icon type="Ionicons" name="settings" style={[styles.IconStyle,{paddingLeft: 4}]} active/>
                            <Text style={styles.TextStyle}>Setting / កំណត់</Text>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={this.handleContactScreen} style={styles.TouchableOpacityStyle}>
                            <Icon active name="call" style={[styles.IconStyle,{paddingLeft: 4}]} active/>
                            <Text style={styles.TextStyle}>Contact / ទំនាក់ទំនង</Text>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={this.handleTosScreen} style={styles.TouchableOpacityStyle}>
                            <Icon active name="star" style={[styles.IconStyle,{paddingLeft: 1}]} active/>
                            <Text style={styles.TextStyle}>Term of use / គោលការណ៏</Text>
                        </TouchableOpacity>

                        <View style={{borderBottomWidth:1, borderBottomColor:'#efefef', paddingTop:15}}></View>
                        <TouchableOpacity onPress={this.handleLogout} style={styles.TouchableOpacityStyle}>
                            <Icon type="Ionicons" name="md-log-out" style={[styles.IconStyle,{paddingLeft: 4}]} active/>
                            <Text style={styles.TextStyle}>Logout / ចាកចេញ</Text>
                        </TouchableOpacity>
                    </View>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    TouchableOpacityStyle: {
        height: 50,
        alignItems: 'center',
        flexDirection: 'row'
    },
    IconStyle: {
        color: Colors.main_color,
        width: 30,
        height: 30
    },
    TextStyle: {
        fontSize:15,
        paddingLeft: 15
    }
});

const mapStateToProps = (state) => {
    return {
        getuserstatus: state.userstatus,
        getUserProfile: state.userprofile,
        getlogout: state.logout,
        userstatus: state.userstatus
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        logout: (data) => dispatch(LogoutActions.logoutRequest(data)),
        updateUserStatus: (data) => dispatch(userStatusActions.userStatusRequest(data)),
        requestUserProfile: (data) => dispatch(UserProfileActions.userProfileRequest(data)),
        requesting: (data) => dispatch(RequestingTypesActions.requestingRequest(data))
    }
}
  
export default connect(mapStateToProps, mapDispatchToProps)(MoreOptionsScreen)