import React,{Component} from 'react';
import {View, TouchableOpacity, StyleSheet, PermissionsAndroid, ToastAndroid, Platform} from 'react-native';
import { Container, Content, Icon, Text, Grid, Col, Row, Input, Button } from 'native-base';
import { Colors, GlobalStyles } from '../Themes';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';

class HomeScreen extends Component{
    constructor(props){
        super(props);
        this.state={
            user_id: ''
        }
    }

    componentDidMount() {
        AsyncStorage.getItem('user_id').then((user_id)=>{
            this.setState({user_id: user_id});
        });
    }

    componentWillUnmount() {

    }

    componentWillReceiveProps = (newProps) =>{
        
    }

    render(){
        return(
            <Container>
                <Content>
                    <Text>Hello world</Text>
                </Content>
            </Container>
        );
    }
}

export default (HomeScreen)