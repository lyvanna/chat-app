import React, { Component } from 'react';
import {View,TouchableOpacity, StyleSheet, Keyboard, BackHandler, Alert, Linking} from 'react-native';
import { connect } from 'react-redux';
import {Input, Spinner, Content, Container, Item, Button, Text, List, ListItem, Left, Body, Icon} from 'native-base';
import { GlobalConst, Colors } from '../Themes';
import setCookieLoginActions from '../Redux/CookieLoginRedux';
import LoginActions from '../Redux/LoginRedux';
import UserProfileActions from '../Redux/UserProfileRedux';
import AsyncStorage from '@react-native-community/async-storage';
import OneSignal from 'react-native-onesignal';
// import RNAccountKit from 'react-native-facebook-account-kit';

class LoginAndRegisterScreen extends Component {
    constructor(props){
        super(props);
        this.state={
            countNumber: 0,
            displayFormLogin: true,
            phoneLogin: '',
            passwordLogin: '',
            errorMessageLogin: false,
            errorTextMessageLogin: '',
            errorMessageRegister: false,
            logoutStatus: null,
            LoadingStatus: false,
            token: null,
            pushkit_token: null,
            player_id: null,
            country_prefix: ''
        }
    }

    handleAlert = () =>
    {
        Alert.alert('Exit App', 'Exiting the application?', [{ text: 'Cancel', onPress: () => console.log('Cancel Pressed'),
        style: 'cancel'}, {text: 'OK',onPress: () => BackHandler.exitApp()}, ], {cancelable: false})
    }

    handleBackButton = () => {
        this.handleAlert();
        return true;
    }

    componentDidMount = () =>{
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        OneSignal.addEventListener('ids', this.onIds);

        // RNAccountKit.configure({
        //     responseType: 'token',
        //     initialPhoneCountryPrefix: '+855',
        //     defaultCountry: 'KH',
        //     countryWhitelist: ['KH']
        // });
    }

    componentWillUnmount() {
        OneSignal.removeEventListener('ids', this.onIds);
    }
    
    onIds =(device) =>{
        this.setState({
            token: device['pushToken'], 
            player_id: device['userId']
        });
    }

    handleLoginButtonPress = () => {
        // RNAccountKit.loginWithPhone()
        // .then((payload) => {
        //     if (!payload) {
        //         // console.log('Login cancelled')
        //     } else {
        //         this.setState({ LoadingStatus: true });
        //         this.props.login({key: GlobalConst.key, token: payload.token});
        //     }
        // });
    }
    
    componentWillReceiveProps = (newProps) =>{
        if (newProps.getValueLogin && newProps.getValueLogin.fetching == false) {
            if (newProps.getValueLogin.payload && newProps.getValueLogin.payload.data) {
                userData = newProps.getValueLogin.payload.data;
                if(userData.user_id){
                    this.props.updateUserToken({
                        key: GlobalConst.key, 
                        user_id: userData.user_id, 
                        token: this.state.token,
                        player_id: this.state.player_id,
                        os: GlobalConst.os
                    });
                    AsyncStorage.setItem('LoginStatus', 'true');
                    AsyncStorage.setItem('user_id', userData.user_id.toString());
                    AsyncStorage.setItem('group_id', userData.group_id.toString());
                    AsyncStorage.setItem('player_id', this.state.player_id);
                    
                    setTimeout(function () {
                        this.props.setcookielogin(true);
                    }.bind(this), 1000);
                }else{
                    this.setState({
                        errorMessageLogin: true, 
                        LoadingStatus: false,
                        errorTextMessageLogin: userData.errorMessage
                    });
                }
            }
        }
    }

    handleTapLogin = ()=>{
        Keyboard.dismiss();
        this.setState({displayFormLogin: true});
    }

    handleForgotPassword = ()=>{
        this.handleLoginButtonPress();
    }

    handleTapRegister= ()=>{
        this.handleLoginButtonPress();
    }

    handlePhoneLogin = (value) => {
        if (/^\d+$/.test(value)) {
            this.setState({
                phoneLogin: value
            });
        }
        if(value=='')
        {
            this.setState({
                phoneLogin: value
            });
        }
    }

    handlePasswordLogin = (value)=>{
        this.setState({
            passwordLogin: value
        });
    }

    handlePressLoginButton = ()=>{
        Keyboard.dismiss();
        this.setState({ 
            errorMessageLogin: false, 
            LoadingStatus: true,
            checkRegisterAction: false
        });
        this.props.login({
            key: GlobalConst.key, 
            phone: this.state.phoneLogin, 
            password: this.state.passwordLogin
        });
    }

    handleTos = ()=>{
        Linking.openURL(GlobalConst.domain+'privacy.html');
    }

    render() {
        return (
            this.state.LoadingStatus?
                <Spinner color="#FFFFFF" />
            :
            <Container>
                <Content>
                    <List style={{padding: 25, paddingLeft: 5, backgroundColor: Colors.primary_color}}>
                        <ListItem noBorder avatar>
                            <Left>
                                <Icon type="Ionicons" name="lock" style={{color:'#fff'}} active />
                            </Left>
                            <Body><Text style={{color:'#fff'}}>Login to {GlobalConst.name}</Text></Body>
                        </ListItem>
                    </List>

                    <View style={{padding: 15}}>
                        <Item>
                            <Input placeholder="phone" keyboardType = 'numeric' value={this.state.phoneLogin} onChangeText={this.handlePhoneLogin} />
                        </Item>
                        <Item>
                            <Input placeholder="password" secureTextEntry={true} value={this.state.passwordLogin} onChangeText={this.handlePasswordLogin} />
                        </Item>

                        <Button block success onPress={this.handlePressLoginButton} style={styles.headerViewLoginRegister}>
                            <Text>LOG IN</Text>
                        </Button>

                        {this.state.errorTextMessageLogin?
                            <Text style={{fontSize: 14, color: 'red', paddingTop:15}}>* {this.state.errorTextMessageLogin}</Text>
                        :null}

                        <View style={styles.headerViewLoginRegisterPassword}>
                            <TouchableOpacity onPress={this.handleForgotPassword}><Text style={styles.TextButtonFogotPassword}>Forgot password?</Text></TouchableOpacity>
                        </View>

                        <Button block danger onPress={this.handleTapRegister} style={styles.headerViewLoginRegister}>
                            <Text>REGISTER</Text>
                        </Button>

                        <View style={styles.headerViewLoginRegister}>
                            <TouchableOpacity onPress={this.handleTos} style={{alignContent:'center', alignItems:'center'}}>
                                <Text style={{fontSize:12, color:'#CCC'}}>By using {GlobalConst.name}, you are agree with our Policy Privacy.</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    headerBeeBus:
    {
        alignItems:'center',
        justifyContent:'center', 
        padding: 30
    },
    TextBeeBus: 
    {
        fontSize:40,
        color:Colors.main_color, 
        fontWeight: 'bold'
    },
    headerViewLoginRegisterTapBar: {
        flexDirection:'row', 
        paddingLeft: 30, 
        paddingRight: 30, 
        justifyContent: 'flex-end', 
        alignItems: 'flex-end'
    },
    headerViewLoginRegister: {
        marginTop: 25
    },
    headerViewLoginRegisterPassword: {
        alignItems:'flex-end',
        justifyContent:'flex-end',
        marginTop: 15
    }
});

const mapStateToProps = (state) => {
    return {
        getValueRegister: state.register,
        getValueLogin: state.login
    }
}
  
const mapDispatchToProps = (dispatch) => {
    return {
        login: (data) => dispatch(LoginActions.loginRequest(data)),
        setcookielogin: (data) => dispatch(setCookieLoginActions.cookieLoginRequest(data)),
        updateUserToken: (data) => dispatch(UserProfileActions.userUpdateProfileRequest(data)),
    }
}
  
export default connect(mapStateToProps, mapDispatchToProps)(LoginAndRegisterScreen)