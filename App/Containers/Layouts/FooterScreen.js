import React, { Component } from 'react';
import { View, AsyncStorage } from 'react-native';
import { Footer, FooterTab, Button, Icon, Text, Badge } from 'native-base';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { GlobalConst, Colors } from '../../Themes';

class FooterScreen extends Component {
  constructor(props){
    super(props);
    this.state={
      withFooter: null,
      transNoticeNum: 0,
      group_id: 0,
      user_id: 0
    }
  }

  componentDidMount = () => 
  {
    AsyncStorage.getItem('user_id').then((user_id)=>{
      this.setState({ 
        user_id: user_id
      });
      this.handleNotif();
    });
  }


  handleHome = ()=>{
    Actions.HomeScreen();
  }

  handleSetting = ()=>{
      Actions.SettingScreen();
  }

  handleNotification = ()=>{
      Actions.NotificationScreen();
  }

  handleMoreOptions = ()=>{
      Actions.MoreOptionsScreen();
  }


  render() {
    if(!this.state.withFooter ) return (<View></View>) 
    if(!this.props.getValueFooter ) return (<View></View>)     
    return (
        <Footer>
          
          <FooterTab style={{backgroundColor:Colors.main_color}}>
            
            <Button vertical onPress={this.handleMoreOptions}>
              <Icon type="Ionicons" name="md-menu" style={{color: '#FFFFFF'}} />
            </Button>
          </FooterTab>
        </Footer>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    getValueFooter: state.setfooter.payload
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
      requestTransNoticeNum: (data) => dispatch(TransNoticeNumActions.transNoticeNumRequest(data))
  }
}

export default (FooterScreen)