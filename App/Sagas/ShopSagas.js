/* ***********************************************************
* A short word on how to use this automagically generated file.
* We're often asked in the ignite gitter channel how to connect
* to a to a third party api, so we thought we'd demonstrate - but
* you should know you can use sagas for other flow control too.
*
* Other points:
*  - You'll need to add this saga to sagas/index.js
*  - This template uses the api declared in sagas/index.js, so
*    you'll need to define a constant in that file.
*************************************************************/

import { call, put } from 'redux-saga/effects'
import ShopActions from '../Redux/ShopRedux'
// import { ShopSelectors } from '../Redux/ShopRedux'

export function * getShop (api, action) {
  const { data } = action
  // get current data from Store
  // const currentData = yield select(ShopSelectors.getData)
  // make the call to the api
  const response = yield call(api.getShop, data)

  // success?
  if (response.ok) {
    // You might need to change the response here - do this with a 'transform',
    // located in ../Transforms/. Otherwise, just pass the data back from the api.
    yield put(ShopActions.shopSuccess(response.data))
  } else {
    yield put(ShopActions.shopFailure())
  }
}



export function * addShop (api, action) {
  const { data } = action
  // get current data from Store
  // const currentData = yield select(ShopSelectors.getData)
  // make the call to the api
  const response = yield call(api.addShop, data)

  // success?
  if (response.ok) {
    yield put(ShopActions.shopSuccess(response.data))
  } else {
    yield put(ShopActions.shopFailure())
  }
}

export function * updateShop (api, action) {
  const { data } = action
  // get current data from Store
  // const currentData = yield select(ShopSelectors.getData)
  // make the call to the api
  const response = yield call(api.updateShop, data)

  // success?
  if (response.ok) {
    yield put(ShopActions.shopSuccess(response.data))
  } else {
    yield put(ShopActions.shopFailure())
  }
}

export function * deleteShop (api, action) {
  const { data } = action
  // get current data from Store
  // const currentData = yield select(ShopSelectors.getData)
  // make the call to the api
  const response = yield call(api.deleteShop, data)

  // success?
  if (response.ok) {
    yield put(ShopActions.deleteShopSuccess(response.data))
  } else {
    yield put(ShopActions.deleteShopFailure())
  }
}

export function * buyShop (api, action) {
  const { data } = action
  // get current data from Store
  // const currentData = yield select(ShopSelectors.getData)
  // make the call to the api
  const response = yield call(api.buyShop, data)

  // success?
  if (response.ok) {
    yield put(ShopActions.buyShopSuccess(response.data))
  } else {
    yield put(ShopActions.buyShopFailure())
  }
}