import { takeLatest, all } from 'redux-saga/effects'
import API from '../Services/Api'
import FixtureAPI from '../Services/FixtureApi'
import DebugConfig from '../Config/DebugConfig'

/* ------------- Types ------------- */

import { StartupTypes } from '../Redux/StartupRedux'
import { RegisterTypes } from '../Redux/RegisterRedux'
import { LoginTypes } from '../Redux/LoginRedux'
import { LogoutTypes } from '../Redux/LogoutRedux'
import { SetFooterTypes } from '../Redux/SetFooterRedux'
import { CookieLoginTypes } from '../Redux/CookieLoginRedux'
import { UserProfileTypes } from '../Redux/UserProfileRedux'
import { TeamTypes } from '../Redux/TeamRedux'
import { startup } from './StartupSagas'
import { register } from './RegisterSagas'
import { login } from './LoginSagas'
import { logout } from './LogoutSagas'
import { getSetFooter } from './SetFooterSagas'
import { getCookieLogin } from './CookieLoginSagas'
import { getuserProfile } from './UserProfileSagas'
import { updateUserProfile } from './UserProfileSagas'
import { getteam } from './TeamSagas'

/* ------------- API ------------- */

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
const api = DebugConfig.useFixtures ? FixtureAPI : API.create()

/* ------------- Connect Types To Sagas ------------- */

export default function * root () {
  yield all([
    // some sagas only receive an action
    takeLatest(StartupTypes.STARTUP, startup),

    // some sagas receive extra parameters in addition to an action
    takeLatest(RegisterTypes.REGISTER_REQUEST, register, api),
    takeLatest(LoginTypes.LOGIN_REQUEST, login, api),
    takeLatest(LogoutTypes.LOGOUT_REQUEST, logout, api),
    takeLatest(SetFooterTypes.SET_FOOTER_REQUEST, getSetFooter),
    takeLatest(CookieLoginTypes.COOKIE_LOGIN_REQUEST, getCookieLogin),
    takeLatest(UserProfileTypes.USER_PROFILE_REQUEST, getuserProfile, api),
    takeLatest(UserProfileTypes.USER_UPDATE_PROFILE_REQUEST, updateUserProfile, api),
    takeLatest(TeamTypes.TEAM_REQUEST, getteam, api)
  ])
}
