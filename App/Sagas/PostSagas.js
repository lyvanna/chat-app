/* ***********************************************************
* A short word on how to use this automagically generated file.
* We're often asked in the ignite gitter channel how to connect
* to a to a third party api, so we thought we'd demonstrate - but
* you should know you can use sagas for other flow control too.
*
* Other points:
*  - You'll need to add this saga to sagas/index.js
*  - This template uses the api declared in sagas/index.js, so
*    you'll need to define a constant in that file.
*************************************************************/

import { call, put } from 'redux-saga/effects'
import PostActions from '../Redux/PostRedux'
// import { PostSelectors } from '../Redux/PostRedux'

export function * getPost (api, action) {
  const { data } = action
  // get current data from Store
  // const currentData = yield select(PostSelectors.getData)
  // make the call to the api
  const response = yield call(api.getPost, data)

  // success?
  if (response.ok) {
    // You might need to change the response here - do this with a 'transform',
    // located in ../Transforms/. Otherwise, just pass the data back from the api.
    yield put(PostActions.postSuccess(response.data))
  } else {
    yield put(PostActions.postFailure())
  }
}

export function * deletePost (api, action) {
  const { data } = action
  // get current data from Store
  // const currentData = yield select(PostSelectors.getData)
  // make the call to the api
  const response = yield call(api.deletePost, data)

  // success?
  if (response.ok) {
    yield put(PostActions.deletePostSuccess(response.data))
  } else {
    yield put(PostActions.deletePostFailure())
  }
}

export function * detailPost (api, action) {
  const { data } = action
  // get current data from Store
  // const currentData = yield select(PostSelectors.getData)
  // make the call to the api
  const response = yield call(api.detailPost, data)

  // success?
  if (response.ok) {
    yield put(PostActions.detailPostSuccess(response.data))
  } else {
    yield put(PostActions.detailPostFailure())
  }
}

export function * myPost (api, action) {
  const { data } = action
  // get current data from Store
  // const currentData = yield select(PostSelectors.getData)
  // make the call to the api
  const response = yield call(api.myPost, data)

  // success?
  if (response.ok) {
    yield put(PostActions.myPostSuccess(response.data))
  } else {
    yield put(PostActions.myPostFailure())
  }
}

export function * addPost (api, action) {
  const { data } = action
  // get current data from Store
  // const currentData = yield select(PostSelectors.getData)
  // make the call to the api
  const response = yield call(api.addPost, data)

  // success?
  if (response.ok) {
    yield put(PostActions.addPostSuccess(response.data))
  } else {
    yield put(PostActions.addPostFailure())
  }
}

export function * updatePost (api, action) {
  const { data } = action
  // get current data from Store
  // const currentData = yield select(PostSelectors.getData)
  // make the call to the api
  const response = yield call(api.updatePost, data)

  // success?
  if (response.ok) {
    yield put(PostActions.updatePostSuccess(response.data))
  } else {
    yield put(PostActions.updatePostFailure())
  }
}

export function * tranPost (api, action) {
  const { data } = action
  // get current data from Store
  // const currentData = yield select(PostSelectors.getData)
  // make the call to the api
  const response = yield call(api.tranPost, data)

  // success?
  if (response.ok) {
    yield put(PostActions.tranPostSuccess(response.data))
  } else {
    yield put(PostActions.tranPostFailure())
  }
}

export function * dropPost (api, action) {
  const { data } = action
  // get current data from Store
  // const currentData = yield select(PostSelectors.getData)
  // make the call to the api
  const response = yield call(api.dropPost, data)

  // success?
  if (response.ok) {
    yield put(PostActions.dropPostSuccess(response.data))
  } else {
    yield put(PostActions.dropPostFailure())
  }
}

export function * geoPost (api, action) {
  const { data } = action
  // get current data from Store
  // const currentData = yield select(PostSelectors.getData)
  // make the call to the api
  const response = yield call(api.geoPost, data)

  // success?
  if (response.ok) {
    yield put(PostActions.geoPostSuccess(response.data))
  } else {
    yield put(PostActions.geoPostFailure())
  }
}

export function * itemPost (api, action) {
  const { data } = action
  // get current data from Store
  // const currentData = yield select(PostSelectors.getData)
  // make the call to the api
  const response = yield call(api.itemPost, data)

  // success?
  if (response.ok) {
    yield put(PostActions.itemPostSuccess(response.data))
  } else {
    yield put(PostActions.itemPostFailure())
  }
}