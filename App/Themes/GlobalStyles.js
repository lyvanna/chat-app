
const GlobalStyles = {
    borderList:
    {
        borderBottomWidth: 1,
        borderBottomColor: '#efefef',
        padding: 5
    },
    bgColor:{
        backgroundColor: '#ffffff',
        flex:1
    },
    frmHolder: {
        padding: 15
    },
    inputBox:{
        borderWidth: 1,
        borderColor: '#E0D7E5'
    },
    frmDiv: {
        paddingTop: 20
    },
    pageTitle: {
        padding: 30, 
        fontSize: 20, 
        backgroundColor: '#035994', 
        color:'#fff'
    },
    closeIcon:{
        width: 30, 
        height: 30, 
        borderRadius: 100, 
        backgroundColor: "red", 
        justifyContent: 'center', 
        alignItems: 'center'
    }
}

export default GlobalStyles
