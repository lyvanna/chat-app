import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  shopRequest: ['data'],
  addShopRequest: ['data'],
  updateShopRequest: ['data'],
  shopSuccess: ['payload'],
  shopFailure: null,
  deleteShopRequest: ['data'],
  deleteShopSuccess: ['payload'],
  deleteShopFailure: null,
  buyShopRequest: ['data'],
  buyShopSuccess: ['payload'],
  buyShopFailure: null

})

export const ShopTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null
})

/* ------------- Selectors ------------- */

export const ShopSelectors = {
  getData: state => state.data
}

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) =>
  state.merge({ fetching: true, data, payload: null })

// successful api lookup
export const success = (state, action) => {
  const { payload } = action
  return state.merge({ fetching: false, error: null, payload })
}

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: false, error: true, payload: null })


export const requestDelete = (state, { data }) =>
  state.merge({ delete: { fetching: true, data, payload: null} })

// successful api lookup
export const successDelete = (state, action) => {
  const { payload } = action
  return state.merge({ delete: { fetching: false, error: null, payload }})
}

// Something went wrong somewhere.
export const failureDelete = state =>
  state.merge({ delete: { fetching: false, error: true, payload: null }})


export const requestBuy = (state, { data }) =>
  state.merge({ buy: { fetching: true, data, payload: null} })

// successful api lookup
export const successBuy = (state, action) => {
  const { payload } = action
  return state.merge({ buy: { fetching: false, error: null, payload }})
}

// Something went wrong somewhere.
export const failureBuy = state =>
  state.merge({ buy: { fetching: false, error: true, payload: null }})

/* ------------- Hookup Reducers To Types ------------- */

// delete

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SHOP_REQUEST]: request,
  [Types.ADD_SHOP_REQUEST]: request,
  [Types.UPDATE_SHOP_REQUEST]: request,
  [Types.SHOP_SUCCESS]: success,
  [Types.SHOP_FAILURE]: failure,
  [Types.DELETE_SHOP_REQUEST]: requestDelete,
  [Types.DELETE_SHOP_SUCCESS]: successDelete,
  [Types.DELETE_SHOP_FAILURE]: failureDelete,
  [Types.BUY_SHOP_REQUEST]: requestBuy,
  [Types.BUY_SHOP_SUCCESS]: successBuy,
  [Types.BUY_SHOP_FAILURE]: failureBuy
})
