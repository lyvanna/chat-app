import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  postRequest: ['data'],
  postSuccess: ['payload'],
  postFailure: null,
  deletePostRequest: ['data'],
  deletePostSuccess: ['payload'],
  deletePostFailure: null,
  detailPostRequest: ['data'],
  detailPostSuccess: ['payload'],
  detailPostFailure: null,
  myPostRequest: ['data'],
  myPostSuccess: ['payload'],
  myPostFailure: null,
  addPostRequest: ['data'],
  addPostSuccess: ['payload'],
  addPostFailure: null,
  updatePostRequest: ['data'],
  updatePostSuccess: ['payload'],
  updatePostFailure: null,
  tranPostRequest: ['data'],
  tranPostSuccess: ['payload'],
  tranPostFailure: null,
  dropPostRequest: ['data'],
  dropPostSuccess: ['payload'],
  dropPostFailure: null,
  geoPostRequest: ['data'],
  geoPostSuccess: ['payload'],
  geoPostFailure: null,
  itemPostRequest: ['data'],
  itemPostSuccess: ['payload'],
  itemPostFailure: null,
})

export const PostTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null
})

/* ------------- Selectors ------------- */

export const PostSelectors = {
  getData: state => state.data
}

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) =>
  state.merge({ fetching: true, data, payload: null })

// successful api lookup
export const success = (state, action) => {
  const { payload } = action
  return state.merge({ fetching: false, error: null, payload })
}

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: false, error: true, payload: null })


export const requestDelete = (state, { data }) =>
  state.merge({ delete: { fetching: true, data, payload: null} })

// successful api lookup
export const successDelete = (state, action) => {
  const { payload } = action
  return state.merge({ delete: { fetching: false, error: null, payload }})
}

// Something went wrong somewhere.
export const failureDelete = state =>
  state.merge({ delete: { fetching: false, error: true, payload: null }})


export const requestDetail = (state, { data }) =>
  state.merge({ detail: { fetching: true, data, payload: null} })

// successful api lookup
export const successDetail = (state, action) => {
  const { payload } = action
  return state.merge({ detail: { fetching: false, error: null, payload }})
}

// Something went wrong somewhere.
export const failureDetail = state =>
  state.merge({ detail: { fetching: false, error: true, payload: null }})

export const requestMy = (state, { data }) =>
  state.merge({ my: { fetching: true, data, payload: null} })

// successful api lookup
export const successMy = (state, action) => {
  const { payload } = action
  return state.merge({ my: { fetching: false, error: null, payload }})
}

// Something went wrong somewhere.
export const failureMy = state =>
  state.merge({ my: { fetching: false, error: true, payload: null }})

export const requestAdd = (state, { data }) =>
  state.merge({ add: { fetching: true, data, payload: null} })

// successful api lookup
export const successAdd = (state, action) => {
  const { payload } = action
  return state.merge({ add: { fetching: false, error: null, payload }})
}

// Something went wrong somewhere.
export const failureAdd = state =>
  state.merge({ add: { fetching: false, error: true, payload: null }})

export const requestUpdate = (state, { data }) =>
  state.merge({ update: { fetching: true, data, payload: null} })

// successful api lookup
export const successUpdate = (state, action) => {
  const { payload } = action
  return state.merge({ update: { fetching: false, error: null, payload }})
}

// Something went wrong somewhere.
export const failureUpdate = state =>
  state.merge({ update: { fetching: false, error: true, payload: null }})

export const requestTran = (state, { data }) =>
  state.merge({ tran: { fetching: true, data, payload: null} })

// successful api lookup
export const successTran = (state, action) => {
  const { payload } = action
  return state.merge({ tran: { fetching: false, error: null, payload }})
}

// Something went wrong somewhere.
export const failureTran = state =>
  state.merge({ tran: { fetching: false, error: true, payload: null }})

export const requestDrop = (state, { data }) =>
  state.merge({ drop: { fetching: true, data, payload: null} })

// successful api lookup
export const successDrop = (state, action) => {
  const { payload } = action
  return state.merge({ drop: { fetching: false, error: null, payload }})
}

// Something went wrong somewhere.
export const failureDrop = state =>
  state.merge({ drop: { fetching: false, error: true, payload: null }})

export const requestGeo = (state, { data }) =>
  state.merge({ geo: { fetching: true, data, payload: null} })

// successful api lookup
export const successGeo = (state, action) => {
  const { payload } = action
  return state.merge({ geo: { fetching: false, error: null, payload }})
}

// Something went wrong somewhere.
export const failureGeo = state =>
  state.merge({ geo: { fetching: false, error: true, payload: null }})

export const requestItem = (state, { data }) =>
  state.merge({ item: { fetching: true, data, payload: null} })

// successful api lookup
export const successItem = (state, action) => {
  const { payload } = action
  return state.merge({ item: { fetching: false, error: null, payload }})
}

// Something went wrong somewhere.
export const failureItem = state =>
  state.merge({ item: { fetching: false, error: true, payload: null }})

/* ------------- Hookup Reducers To Types ------------- */

// delete

export const reducer = createReducer(INITIAL_STATE, {
  [Types.POST_REQUEST]: request,
  [Types.POST_SUCCESS]: success,
  [Types.POST_FAILURE]: failure,
  [Types.DELETE_POST_REQUEST]: requestDelete,
  [Types.DELETE_POST_SUCCESS]: successDelete,
  [Types.DELETE_POST_FAILURE]: failureDelete,
  [Types.DETAIL_POST_REQUEST]: requestDetail,
  [Types.DETAIL_POST_SUCCESS]: successDetail,
  [Types.DETAIL_POST_FAILURE]: failureDetail,
  [Types.MY_POST_REQUEST]: requestMy,
  [Types.MY_POST_SUCCESS]: successMy,
  [Types.MY_POST_FAILURE]: failureMy,
  [Types.ADD_POST_REQUEST]: requestAdd,
  [Types.ADD_POST_SUCCESS]: successAdd,
  [Types.ADD_POST_FAILURE]: failureAdd,
  [Types.UPDATE_POST_REQUEST]: requestUpdate,
  [Types.UPDATE_POST_SUCCESS]: successUpdate,
  [Types.UPDATE_POST_FAILURE]: failureUpdate,
  [Types.TRAN_POST_REQUEST]: requestTran,
  [Types.TRAN_POST_SUCCESS]: successTran,
  [Types.TRAN_POST_FAILURE]: failureTran,
  [Types.DROP_POST_REQUEST]: requestDrop,
  [Types.DROP_POST_SUCCESS]: successDrop,
  [Types.DROP_POST_FAILURE]: failureDrop,
  [Types.GEO_POST_REQUEST]: requestGeo,
  [Types.GEO_POST_SUCCESS]: successGeo,
  [Types.GEO_POST_FAILURE]: failureGeo,
  [Types.ITEM_POST_REQUEST]: requestItem,
  [Types.ITEM_POST_SUCCESS]: successItem,
  [Types.ITEM_POST_FAILURE]: failureItem
})
