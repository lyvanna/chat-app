import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  driversRequest: ['data'],
  acceptedCallRequest: ['data'],
  cancelCallRequest: ['data'],
  arrivedRequest: ['data'],
  startRequest: ['data'],
  endRequest: ['data'],
  calculateRequest: ['data'],
  driversSuccess: ['payload'],
  driversFailure: null,
})

export const DriversTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null
})

/* ------------- Selectors ------------- */

export const DriversSelectors = {
  getData: state => state.data
}

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) =>
  state.merge({ fetching: true, data, payload: null })

// successful api lookup
export const success = (state, action) => {
  const { payload } = action
  return state.merge({ fetching: false, error: null, payload })
}

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: false, error: true, payload: null })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.DRIVERS_REQUEST]: request,
  [Types.ACCEPTED_CALL_REQUEST]: request,
  [Types.CANCEL_CALL_REQUEST]: request,

  [Types.ARRIVED_REQUEST]: request,
  [Types.START_REQUEST]: request,
  [Types.END_REQUEST]: request,
  [Types.CALCULATE_REQUEST]: request,

  [Types.DRIVERS_SUCCESS]: success,
  [Types.DRIVERS_FAILURE]: failure,
})
