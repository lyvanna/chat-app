import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  luckyDrawRequest: ['data'],
  luckyDrawSuccess: ['payload'],
  luckyDrawFailure: null,
  luckyDrawJoinRequest: ['data'],
  luckyDrawJoinSuccess: ['payload'],
  luckyDrawJoinFailure: null
})

export const LuckyDrawTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null
})

/* ------------- Selectors ------------- */

export const LuckyDrawSelectors = {
  getData: state => state.data
}

/* ------------- Reducers ------------- */

// request the data from an api
export const requestIndex = (state, { data }) =>
  state.merge({ index: { fetching: true, data, payload: null } })

// successful api lookup
export const successIndex = (state, action) => {
  const { payload } = action
  return state.merge({ index: { fetching: false, error: null, payload } })
}

// Something went wrong somewhere.
export const failureIndex = state =>
  state.merge({ index: { fetching: false, error: true, payload: null } })

export const requestJoin = (state, { data }) =>
  state.merge({ join: { fetching: true, data, payload: null } })

// successful api lookup
export const successJoin = (state, action) => {
  const { payload } = action
  return state.merge({ join: { fetching: false, error: null, payload } })
}

// Something went wrong somewhere.
export const failureJoin = state =>
  state.merge({ join: { fetching: false, error: true, payload: null } })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.LUCKY_DRAW_REQUEST]: requestIndex,
  [Types.LUCKY_DRAW_SUCCESS]: successIndex,
  [Types.LUCKY_DRAW_FAILURE]: failureIndex,
  [Types.LUCKY_DRAW_JOIN_REQUEST]: requestJoin,
  [Types.LUCKY_DRAW_JOIN_SUCCESS]: successJoin,
  [Types.LUCKY_DRAW_JOIN_FAILURE]: failureJoin
})
