import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  geoLocationRequest: ['data'],
  geoLocationSuccess: ['payload'],
  geoLocationFailure: null
})

export const GeoLocationTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null
})

/* ------------- Selectors ------------- */

export const GeoLocationSelectors = {
  getData: state => state.data
}

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) =>
  state.merge({ fetching: true, data, payload: null })

// successful api lookup
export const success = (state, action) => {
  const { payload } = action
  return state.merge({ geo: { fetching: false, error: null, payload } })
}

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ geo: {fetching: false, error: true, payload: null } })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GEO_LOCATION_REQUEST]: request,
  [Types.GEO_LOCATION_SUCCESS]: success,
  [Types.GEO_LOCATION_FAILURE]: failure
})
