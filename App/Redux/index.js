import { combineReducers } from 'redux'
import configureStore from './CreateStore'
import rootSaga from '../Sagas/'

/* ------------- Assemble The Reducers ------------- */
export const reducers = combineReducers({
  nav: require('./NavigationRedux').reducer,
  register: require('./RegisterRedux').reducer,
  login: require('./LoginRedux').reducer,
  logout: require('./LogoutRedux').reducer,
  setfooter: require('./SetFooterRedux').reducer,
  cookielogin: require('./CookieLoginRedux').reducer,
  userprofile: require('./UserProfileRedux').reducer,
  updateprofile: require('./UserProfileRedux').reducer,
  getteam: require('./TeamRedux').reducer,
  userstatus: require('./UserStatusRedux').reducer
})

export default () => {
  let { store, sagasManager, sagaMiddleware } = configureStore(reducers, rootSaga)

  if (module.hot) {
    module.hot.accept(() => {
      const nextRootReducer = require('./').reducers
      store.replaceReducer(nextRootReducer)

      const newYieldedSagas = require('../Sagas').default
      sagasManager.cancel()
      sagasManager.done.then(() => {
        sagasManager = sagaMiddleware.run(newYieldedSagas)
      })
    })
  }

  return store
}
