import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  chatFcmRequest: ['data'],
  chatFcmSuccess: ['payload'],
  chatFcmFailure: null,
  chatCameraRequest: ['data'],
  chatCameraSuccess: ['payload'],
  chatCameraFailure: null
})

export const ChatTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null
})

/* ------------- Selectors ------------- */

export const ChatSelectors = {
  getData: state => state.data
}

/* ------------- Reducers ------------- */

// request the data from an api
export const requestFcm = (state, { data }) =>
  state.merge({ chatFcmData: { fetching: true, data, payload: null } })

// successful api lookup
export const successFcm = (state, action) => {
  const { payload } = action
  return state.merge({ chatFcmData: { fetching: false, error: null, payload } })
}

// Something went wrong somewhere.
export const failureFcm = state =>
  state.merge({ chatFcmData: { fetching: false, error: true, payload: null } })

export const requestCamera= (state, { data }) =>
  state.merge({ chatCamera: { fetching: true, data, payload: null } })

// successful api lookup
export const successCamera= (state, action) => {
  const { payload } = action
  return state.merge({ chatCamera: { fetching: false, error: null, payload } })
}

// Something went wrong somewhere.
export const failureCamera= state =>
  state.merge({ chatCamera: { fetching: false, error: true, payload: null } })


/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.CHAT_FCM_REQUEST]: requestFcm,
  [Types.CHAT_FCM_SUCCESS]: successFcm,
  [Types.CHAT_FCM_FAILURE]: failureFcm,
  [Types.CHAT_CAMERA_REQUEST]: requestCamera,
  [Types.CHAT_CAMERA_SUCCESS]: successCamera,
  [Types.CHAT_CAMERA_FAILURE]: failureCamera
})
